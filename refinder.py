#!/usr/bin/env python3

# ------------------------- ¡WARNING! --------------------------- #
# Este script requiere Python3 para funcionar. Por favor, verefica
# que esté instalado en el sistema
# --------------------------------------------------------------- #

#Importo los módulos que voy a usar
import os
import re
import sys
import time

from os import path
from shutil import copyfile



# ------------------------- DECLARACIÓN DE FUNCIONES  --------------------------- #



def usage():
	print('Para usar este script, debes llamar al script usando como argumento un archivo fasta')
	print('Recuerda: sólo UN argumento está permitido, y debe ser un fichero FASTA')
	sys.exit(1)

def licencia():
	print('© 2020 Pablo Ignacio Marcos López')
	print()
	print('This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.')
	print()
	print('This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.')
	print()
	print('You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.')
	print()
	print('Coded with GNU Nano')
	sys.exit(0)

def check_input(entry):
	if path.exists(entry):
		#Abre el archivo, lo lee y lo asigna a variable
		read = open(entry).read()
		#Comprobemos que el archivo sea tipo fasta
		if '>' != read[0]:
			print('Este archivo no sigue el formato FASTA')
			usage()
		else:
			#Llevo a cabo un procesado del archivo para obtener sólo la secu>
			noheader = open(entry).readlines()[1:]
			DNA = ''.join(noheader).replace('\n', '')
			return(DNA)
	else:
		print('Este archivo no existe')
		sys.exit(1)



# ------------------------- CONTROL DE ARGUMENTOS --------------------------- #



if len(sys.argv) != 2:
	print('ERROR: ', end='')
	usage()
elif (sys.argv[1] == '-h' ) or (sys.argv[1] == '--help'): 
	print('Vaya, necesitas una ayudita?')
	usage()
elif (sys.argv[1] == '--license') or (sys.argv[1] == '--copyright'):
        licencia()
elif(sys.argv[1].startswith('-')) or (sys.argv[1].startswith('--')):
	print('Error:', sys.argv[1], 'no es una opción de este programa')
	usage()
else:
	print('Bienvenidx a refinder_rewritten')



# ------------------------- INTRODUCCIÓN --------------------------- #



print('Este script toma una secuencia de DNA como argumento y le aplica unas enzimas'
      'de restricción, obteniendo un output con las distintos fragmentos generados')

#Se comprueba el archivo input usando la función dada
DNA = check_input(sys.argv[1])

print('')
#Se pide un directorio de trabajo
print('Para dejalo todo ordenadito, especifica un directorio de trabajo para el output')

workdir = input()
try:
	os.mkdir(workdir)
	print('Directorio de trabajo creado')
except:
	print('Problema durante la creación del directorio. ¿Ha comprobado que no exista?')
	sys.exit(1)
print('')



# ------------------------- CORTE DE LAS SECUENCIAS --------------------------- #



#Output que muestra al usuario los parámtros establecidos
print('Se han establecido los siguientes parámetros:')
print('*******************  Parámetros  *******************')
print('Archivo DNA de lectura:\t\t',sys.argv[1])
print('Enzima de Restricción AbcI:\t Corte en ANT*AAT')
print('Enzima de Restricción AbcII:\t Corte en GCRW*TG')
print('Directorio de trabajo:\t\t',workdir) 
print('****************************************************')

print('')
print('Preparando las tijeras...')

time.sleep(1) #Esto no es necesario, pero me monto mi película
print('Cortando con enzima: AbcI')
#Ojo con esto: Es un regex con lookahead. Muy elegante, imho
primercorte = re.split('A[ATCG]T(?=AAT)|GC[AG][AT](?=TG)', DNA, flags=re.IGNORECASE)

#Al principio iba a hacer dos cortes, pero finalmente lo he hecho todo junto.
#No he podido resistirme a dejarlo, pues quedana muy mono el STOUT
time.sleep(1)
print('Cortando con enzima: AbcII')
time.sleep(1)



# ------------------------- GUARDADO DEL OUTPUT --------------------------- #



with open('output.tsv', 'w') as salida:
	salida.write('Longitud del Fragmento\tSecuencia')
	longitudtotal = 0
	for i in range(len(primercorte)):
		longitudtotal = longitudtotal + len(primercorte[i])
		salida.write(''.join((str(len(primercorte[i])),'\t',str(primercorte[i]))))
print('Número total de fragmentos:',len(primercorte))
print('Longitud promedio de los fragmentos:', int(longitudtotal/len(primercorte)),'bases' )

print('')
print('Puedes consultar su secuencia en ./{}/output.tsv, donde también he guardado el archivo proporcionado como ìnput'.format(workdir))

os.replace('output.tsv', './{}/output.tsv'.format(workdir))
copyfile('{}'.format(sys.argv[1]), './{}/input.tsv'.format(workdir))
print('Recosiendo el ADN, Guardando tijeras, Inactivando enzimas...')
