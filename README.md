# refinder-rewritten

Este script toma una secuencia de DNA como argumento y le aplica unas enzimas de restricción, obteniendo un output con las distintos fragmentos generados, que se guardan en una carpeta definida por el usuario.

## Descarga
Para usar este método, es necesario tener instalado git en el sistema (`sudo apt install git-all`)
1. Clona este repositorio: `git clone https://codeberg.org/FlyingFlamingo/refinder-rewritten`
2. Entra en el directorio: `cd refinder-rewritten`
3. Ejecútalo como `python3 refinder.py fasta.fa`, donde fasta.fa es tu archivo fasta unisecuencial

Recuerda que tener instalado python3 es un prerrequisito para este script

## Opciones
Al llamar al script, tenemos las siguientes opciones:

| Opción | Descripción |
| -------- | -------- |
| ``--help | -h`` | Muestra la ayuda |
| ``--license | --copyright`` | Muestra la licencia |


